# Reversing

Do you ever feel like your conversations with prospects end too soon?

Have you ever said the *wrong thing* and ended the meeting too quickly, before you've even had a chance to prove your value?

The reason why this happens is that most SDRs deploy product knowledge incorrectly.

They're so excited to learn about the product and want to share what they know...

That they forget that it's not about them, but about their prospect.

It's easy to talk about what your product does, the awards/accolades it's gotten, and who your other customers are.

But that information doesn't have any value of it's own. It's just noise.

Prospects don't care about your features, they're skeptical of your hyperbole, and your social proof is nice to know, but not necessary.

They care about how you can solve a problem that they have.

When you don't take the time to ask questions... when you're too busy vomiting information about your product... when you don't establish the context...

Prospects have to work harder to understand what you do, which inevitably leads to comparing you to your competitors, which commoditizes your value proposition.

What's worse, you don't get to learn what this prospect actually cares about... the crucial piece of information that makes the sale easy.

The solution is simple.

Learn and practice how to Reverse their questions.

Instead of immediately answering a prospect's question with product knowledge, **get curious**.

"You must have asked that for a reason, is that important to you?"

"When you say X... could you tell me a little bit more about that?"

Ask questions instead of giving out answers.

It's about Prospect Knowledge, not Product Knowledge.

****