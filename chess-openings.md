# Chess Openings

I almost always open with the French Defense when I play Chess.


I've gotten to know it over the years, and can anticipate most responses.


This gives me a home field advantage. 


It allows me to focus my energy and creativity on developing the direction of the middle and endgame.


Why is this important? I'm here to argue that SDRs need to develop their openings.


I've seen SDRs jump into a call without a plan and blurt out whatever comes to mind.


"Hey this is SDR from Company... do you have a minute?"


"Hey this is SDR... I know I'm catching you out of the blue, is now a good time?"


These are the openings that SIGNAL to the prospect that a) this is a cold-call, b) this person is going to take up a lot of my time, and c) I need to get off the phone as quickly as I can.


When you open a call this way, you've handed control over and you're on the back-step.


Novice SDRs react to whatever their prospect is doing. 


There's no framework, no methodology, no strategy behind the call.


If instead you practiced simple and proven call openings, you would control the conversation, and focus your energy and attention on uncovering and solving your prospect's pain.


That's the magic of opening. That's why I'm a big fan of pattern interrupts.


How do you open your calls?


#salesdevelopment

posted on 2020-07-26