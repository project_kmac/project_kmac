# SDRs: Why Efficiency is More Important Than Ever

## Introduction

> Companies with recurring revenue models, “software margins” (usually considered gross margins of 70% or better), and negative net churn (where expansion revenue growth outstrips any gross account churn) are extremely resilient in general. In difficult market conditions, the companies who can respond to the changing market and take this opportunity to tighten up operations stand to benefit.
> -- Danielle Morrill, "A Ticker a Day"

Brace yourselves, the Great Depression of our generation is coming. Whether by pandemic, productivity, or inflation, it's already here. We are already seeing the leading indicators, but simply because there's a lag time for the full effects to arrive, does not mean we are in the clear.

## Companies with an Inefficient Go-To-Market Will Lose

Those who simply throw more bodies (i.e. hire more salespeople) to solve the problem won't make it. Those who rely solely on outdated marketing models will see poorer Customer Acquisition Costs. The SDR teams that operate inefficiently will suffer and will be an area of high-turnover.

What happens in a downturn? Customers are less willing/able to spend money. Which leads to fewer sales, and ultimately a reduced marketing budget.

The SDR teams that rely heavily on spray-and-pray and mass-blast will quickly find themselves sinking. 

The SDR teams that have a predictable methodology and repeatable system, who are efficient in their use of tools, who have a streamlined process for converting prospect data into qualified meetings...

## I've obsessed over SDR efficiency the past two years

It was a visceral pain point because it affected my day to day experience.

Here's what will give you the most leverage, and create elite SDRs that can deliver results that are a multiple of most teams:

(Disclaimer. I'm religious about efficiency. You may not want to take things to the same extreme.)


* **Prospect Data First.** SDRs shouldn't be spending an inordinate amount of time researching prospects and assembling lead lists. Also, why spend the licensing costs on seats for ALL SDRs on your team, when they only use ZoomInfo for a small part of their day? (I've personally tracked this. When I was actively engaged in outbound, there were days and weeks I didn't need to touch DiscoverOrg. I'd go back in once a month or so to do a list pull. That's very little utilization for a tool that costs so much.)

* **Email Sequence Segmentation.** If you want to prevent mass-blasting, you need to make doing the right thing the easiest thing, the path of least resistance. How? Turn-key email sequences, ready to go for each buyer persona they encounter. This way, they're not dropping 10+ contacts from the same company, with different roles and concerns, into the same generic email template. When you let SDRs write their own emails, they'll default to copying messaging anyway.

* **Call Volume.** I've been guilty of taking too much time in between calls. My best performances came from when I got real efficient with my calls. That means minimizing the downtime between calls and negating context switching. Use the Email Sequence Segmentation to call only one buyer persona at a time. That way you're delivering the same message, and not having to think about what to say when someone does pick up. I used nearly identical value propositions in my calls, to what I had written in my email. Use browser extensions like Vimium to reduce mouse/touchpad usage, and be able to dial calls using keyboard only. Open multiple Outreach tabs at a time, to log and start calls at the same time. (As an example, open up a sequence for a single buyer persona, open 10 prospects in their respective tabs, start dialling the first, switch to the second tab and dial, wait, close and log the first call if it doesn't answer, rinse and repeat until you get an answer. When you do have an answer, your pattern interrupt and upfront contract allows you enough time to open up that person's LinkedIn and find a premise for personalization/earntheright.)

* **Digital** 