# Lost Tourist Method - aka Pattern Interrupts, pt 2
What are your favourite ways to open a call?



I have three pattern interrupts that I rotate, depending on where my energy level is and what intuition tells me about the person on the other end.



See, I have this weird belief...



I don't believe in smiling and dialing. 



I believe that you don't have to be high energy, and you don't have to always be excited when you call.



Why? What are you so excited about?



It's a dead giveaway that you're trying to manipulate my emotions so I buy something.



Here's how I open my calls:



"Hi {{first.name}}, this is Kevin... I'm looking for a little help."



"Hi {{first.name}}, this is Kevin... Does my name sound at all familiar to you?"



"Hi {{first.name}}, this is Kevin... How have you been?"



Just those three. Pretty simple. 



Notice that there's almost always a pause after I introduce myself. 



And typically, I sound like I'm lost and looking for directions.



Because I AM looking for help, and I don't know if they've ever heard of me, and I DO want to know how they've been)



I've built a million dollar pipeline in a completely greenfield enterprise territory on the back of those three openers.



I'm curious to know... what do you sound like when you open your calls?

****

Just an honest conversation between two human beings.

Are you doing the high energy thing? How's it working for you?

(Yes, intuition. That's the Art in the Art and Science of SDR)

I call it the Lost Tourist Method. Buy my course for $420.69. (jk)

