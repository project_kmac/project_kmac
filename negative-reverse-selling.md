# How to FIX Commission Breath

SDRs: I KNOW you HATE being pushy and manipulative.

And yet,  every salesperson I've ever met was taught to "handle objections", "go for the close," and "build a 'Yes' ladder" to get the prospect to agree.

Sales people are taught that they have to be pushy to close deals.

They try to sell using positive attributes, spend a lot of energy showing their excitement, and basically seek to arm wrestle prospects into accepting a meeting or a deal.

What if there was an easier, softer way?

A method that allows you to sell from a place of authenticity.

The secret is called "Negative Reverse Selling"

And this means you NEVER try to overcome or convince your prospect of anything.

Instead you LEAN IN and AGREE with their concerns and objections.

If you know there's something that's going to be an issue, you bring it up first.

You don't hide the flaws of your product, you talk about them first.

Instead of trying to get them to say Yes, you give them every opportunity to say No.

When you try to get someone to change their mind, they will defend their choice.

When you give them every opportunity to say No, you offer freedom to choose.

This shift in philosophy has made the biggest difference in my craft.

*****