# Pattern Interrupts

Have you ever had a cold-call where your prospect hung up before you even got to your pitch?

There's a simple solution.

Using a Pattern Interrupt to open your cold calls gives you control of the conversation.

It allows you to side-step the brush-offs from prospects wanting to drop off the call.

Here's the thing.

When your tonality is off and you sound like every cold-call they've ever gotten...

You end up in their mental spam filter.

So it doesn't matter that your product is the best thing since sliced bread.

It doesn't mean anything that your service is exactly what they need to solve their problem.

None of your value propositions will get through.

So you owe it to yourself, and you owe it to your prospect, to move the call past the knee-jerk reaction so you can have a productive conversation.

What pattern interrupts are you using?