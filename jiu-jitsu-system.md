# The System, pt 1. Danaher

WIP

John Danaher, arguably the greatest mind in Brazilian Jiu-Jitsu, defines jiu-jitsu as a four step system. 

Step 1. Get the fight to the ground. 
Step 2. Pass the opponent's legs. 
Step 3. Get control through a hierarchy of pins.
Step 4. Attack with a submission.

And that's it. Ground. Pass. Control. Submit.

When executed well and consistently leads to brilliant results. (See the Danaher Death Squad)

How's this relevant to sales and marketing? I'm glad you asked.

There's a lot of sales development advice out there, some of it under the guise of best practices.

You know the type, "Send emails with THIS subject line", "Make calls at this time of day", or "This LinkedIn Message gets CRAZY results"... whatever.

These tactics will almost never work for you off the shelf.

Reason being that they're tactics that work within a particular environment, with a specific set of accounts, or some sort of variable that's not included in the directions.

Whenever I see a tactic, I try to evaluate it within the context of it's system. 

Heck, anything that I say, any tactic is only valuable within the context of my system. So evaluate accordingly.

It's in the system where the value lies.

How would you describe your Sales Development System?