# Probability of Calls

Ever been reluctant to pick up the phone?

SDRs get too attached to the outcome and they hesitate to dial because their ego gets involved.

There's a simple solution.

Make cold calls like you play blackjack.

Here's what I mean...

In blackjack, it's not about winning any single hand. It's about playing a system, and having an edge in probability over a period of time.

In the same way, it's not about any single call.

When you approach cold-calling on a call-by-call basis, every call becomes life or death.

This creates the pressure of expectations which forces you into mistakes and leads to commission breath.

It's kind of like betting the family farm on a single hand of blackjack. You HAVE to win that hand.

If instead you view it as the result of a series-of-calls, you accept any outcome fearlessly.

You're free to make as many calls as you want, because your ego is no longer attached.

You make calls with zero expectations, trusting that the system will lead to the right results.

And you end up winning.