# What SDRs need do to in the first 30 days

Pains
	Don't know where to start/Don't know who to go after
	Value proposition is too broad/doesn't resonate
	Not getting enough meetings
	Spending too much time on admin work

Cartography of Territory
	Title/Role in ZoomInfo
	Keywords in LISN
	Sell to the evangelists, not the late adopters
	Spectrum of Awareness

Segmentation
	Why is it so hard for SDRs to personalize? Why is it so easy to mass blast?
	Marketing sending the same email to large lists - SDRs modeling that behaviour
	Specific Value Propositions, Pain Points, Messaging to each persona
	A separate sequence per each Value Stream
	Part of the reason why we're not iterating on messaging, is because of the reliance on sending the same message

End Result
	Spend more time on calls/emails, instead of foundational/administrative work (list uploads, lead research, etc.)
	Allows you to prioritize who you're going after (by level of awareness, by value stream, etc.)
	Less context switching - which means flow 
	Faster improvement, more meetings


## LI Copy


SDRs: If you've ever found yourself frustrated because you're spending too much time doing admin work (like list uploads, lead research, etc), then watch on...

The problem is that list uploads/lead research doesn't get you paid. 

As an SDR you're compensated on meetings set and Sales Accepted Opportunities.

Because of this, highest and best use of your time is having conversations with prospects. 

So for me, I wanted to do as much of what gets me paid, and as little as possible of the admin work.

Which meant getting finding ways to optimize and be more efficient. 

If you want to do more calls, send more emails, and book more meetings, this is what I would do first.

These are the TWO things every SDR needs to do in their first 30 days to be set up for success.

If you do this, it also allows you to prioritize who you're going after to have a better conversion rate.

And that means new SDRs can improve faster, and book more meetings.

Cheers,
KMac