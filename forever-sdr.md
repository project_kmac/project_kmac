# Forever SDR

Every SDR I've ever talked to got into the job as a stepping stone toward becoming an AE.

Not necessarily because they have a love for sales, but because that's where the big money is.

That's a recipe for unhappiness.

I've been asked before when I would "move up" into a closing role.

Let me say it clearly for the record.

I have ZERO INTEREST in becoming an Account Executive.

I've had opportunities to move into a closing role and didn't take them.

Here's why.

I've seen the underlying dynamic that Account Executives (AEs) are dependent on the inbound leads that marketing generates.

No matter how good I am in sales, the bottleneck is in how full my pipeline is.

I can increase my closing ratio, I can try influence my average deal size, I can even try to push for faster time-to-close...

But those are incremental improvements.

The biggest leverage I have in sales is having a full pipeline.

That's why I'm betting my career on sales development.

Creating a pipeline from scratch, building that something from nothing... that's my craft. 

<!-- I'm the best in the world at it. And I'm still getting better and honing my craft everyday.  -->

I want this forever.